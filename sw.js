var CACHE_NAME = 'DP-v1.1.3';
var urlsToCache = [
	'index.html',
	'privacy.html',
	'img/add.svg',
	'img/folder.svg',
	'img/og.png',
	'img/back.svg',
	'img/footer-pattern.png',
	'img/reload.svg',
	'img/google.svg',
	'img/search.svg',
	'img/delete.svg',
	'img/info.svg',
	'img/trash.svg',
	'img/edit.svg',
	'img/link.svg',
	'img/twitter.svg',
	'img/edit2.svg',
	'img/facebook.svg',
	'img/loading.gif',
	'img/logout.svg',
	'css/style.css',
	'css/dash.css',
	'css/40em.css',
	'bower_components/jquery/dist/jquery.min.js',
	'bower_components/jquery-touchswipe/jquery.touchSwipe.min.js',
	'js/main.js',
	'js/dash.js',
	'json/un.json',
	'json/countries.json',
	'json/resources.json',
	'json/unLinks.json',
	'json/ngoCategories.json',
	'json/topic.json',
	'json/ngos.json',
	'json/treaties.json',
	'json/res/ga.json',
	'json/res/sc.json',
	'json/res/sg.json'
];

self.addEventListener('install', function(event) {
	event.waitUntil(
		caches.open(CACHE_NAME)
			.then(function(cache) {
				return cache.addAll(urlsToCache);
			})
	);
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        if (response) {
          return response;
        }
        return fetch(event.request);
      }
    )
  );
});