///// Dashboard

/// Folder Functions
// Folders List
function getFolders(){
	$("#folders").css("cursor","initial");
	$("#navbar").addClass("hidden");
	$(".folder").css("cursor","pointer");
	$("body").css("cursor","initial");
	$("#delete-button").prop("disabled", false);
	gapi.client.drive.files.list({
		spaces: "appDataFolder",
		orderBy: "createdDate"
	}).then(function(response) {
		$("#folders").removeClass("loading");
		$("#links").css("display","none");
		$("#folders").css("display","block");
		$("#folder-options").css("display","none");
		$("#dash-title").html("Dashboard");
		$("#dash-title").removeClass("links");
		$("#folders").html("");
		folders = response.result.items;
		$("#folders").append(
			'<li id="essentials" onclick="essentials()">'+
			'<span class="folder-text">Essentials</span></li>'
		);
		for(item in response.result.items){
			$("#folders").append(
				'<li class="folder" folder-id="'+
				item+'"><span class="folder-text" '+
				'contenteditable="true">'+
				folders[item].title+'</span></li>'
			);
		}
		$("#folders").append(
			'<li id="add-folder" title="Add a folder">'+
			'<form id="folder-details" style="display:none;" '+
			'action="#"><input type="text" '+
			'placeholder="Folder Name" /><button type="submit">'+
			'Create Folder</button></form></li>'
		);
		$(".folder").one("click", function(){
			folderId = $(this).attr("folder-id");
			getLinks(folderId);
		});
		$("#add-folder").one("click",function(){
			addFolder();
		});
		$(".folder").off("dragover").on("dragover",function(event){
			event.preventDefault();
			$(this).addClass("dragging");
		});
		$(".folder").off("dragleave").on("dragleave",function(event){
			event.preventDefault();
			window.open("https://www.google.com/");
			$(this).removeClass("dragging");
		});
		$(".folder").off("drop").on("drop",function(){
			event.preventDefault();
			$(this).removeClass("dragging");
			folderId = $(this).attr("folder-id");
			var result = event.dataTransfer.getData("Text");
			var linktitle = draggingLinkText || result;
			if(result !== "" && isUrl(result)){
				$("#folders").css("cursor","progress");
				$(".folder").css("cursor","progress");
				$("body").css("cursor","progress");
				var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
				$.ajax({
					url: folders[folderId].downloadUrl,
					dataType: "json",
					headers: {
						"Authorization": "Bearer " + accessToken
					}
				}).done(function(links){
					links.push({
						name: linktitle,
						url: result
					});
					var base64Data = btoa(JSON.stringify(links));
					var contentType = "application/json";
					const boundary = '-------314159265358979323846';
					const delimiter = "\r\n--" + boundary + "\r\n";
					const close_delim = "\r\n--" + boundary + "--";
					var multipartRequestBody =
									delimiter +
									'Content-Type: application/json\r\n\r\n' +
									JSON.stringify(folders[folderId]) +
									delimiter +
									'Content-Type: ' + contentType + '\r\n' +
									'Content-Transfer-Encoding: base64\r\n' +
									'\r\n' +
									base64Data +
									close_delim;

					gapi.client.request({
						'path': '/upload/drive/v2/files/' + folders[folderId].id,
						'method': 'PUT',
						'params': {'uploadType': 'multipart', 'alt': 'json'},
						'headers': {
							'Content-Type': 'multipart/mixed; boundary="' + boundary + '"'
						},
						'body': multipartRequestBody
					}).then(function(){
						getLinks(folderId);
					});
				});
			}
		});
		$(".folder-text").off("click").on("click",function(event){
			event.stopPropagation();
			$(this).off("blur").on("blur",function(){
				renameFolder(this);
			});
		});
	});
}

// New Folder
function addFolder(){
	toggleCover(true);
	$("#add-folder").addClass("adding-folder");
	$("#cover").unbind("click").click(function(){
		toggleCover(false);
		$("#add-folder").removeClass("adding-folder");
		getFolders();
	});
	$("#folder-details").one("submit",function(event){
		event.preventDefault();
		var folderName = $("#folder-details input").val();
		var contentType = 'application/json';
		const boundary = '-------314159265358979323846';
		const delimiter = "\r\n--" + boundary + "\r\n";
		const close_delim = "\r\n--" + boundary + "--";
		var metadata = {
			title: folderName,
			mimeType: "application/json",
			parents: [{id: "appDataFolder"}]
		}
		var base64Data = "W10=";
		var multipartRequestBody = delimiter + 'Content-Type: application/json\r\n\r\n' + JSON.stringify(metadata) + delimiter + 'Content-Type: ' + contentType + '\r\n' + 'Content-Transfer-Encoding: base64\r\n' + '\r\n' + base64Data + close_delim;

		gapi.client.request({
			'path': '/upload/drive/v2/files',
			'method': 'POST',
			'params': {'uploadType': 'multipart'},
			'headers': {
				'Content-Type': 'multipart/mixed; boundary="' + boundary + '"'
			},
			'body': multipartRequestBody
		}).then(function() {
			toggleCover(false);
			$("#add-folder").removeClass("adding-folder");
			$("#folders").css("cursor","progress");
			$(".folder").css("cursor","progress");
			$("body").css("cursor","progress");
			getFolders();
		});
	});
}

// Edit Open Folder
function editOpenFolder(){
	toggleCover(true);
	$("#folder-edit").css("display", "block");
	$("#folder-edit .folderName").val(folders[folderId].title);
	$("#cover").unbind("click").click(function(){
		toggleCover(false);
		$("#folder-edit").css("display", "none");
	});
	$("#folder-edit").one("submit",function(event){
		event.preventDefault();
		var newTitle = $(".folderName").val();
		if(newTitle !== undefined){
			var body = {'title': newTitle};
			gapi.client.drive.files.patch({
				'fileId': folders[folderId].id,
				'resource': body
			}).then(function(){
				toggleCover(false);
				$("#folder-edit").css("display", "none");
				gapi.client.drive.files.list({
					spaces: "appDataFolder",
					orderBy: "createdDate"
				}).then(function(response) {
					folders = response.result.items;
					$("#dash-title").html(newTitle);
				});
			});
		}
	});
}

// Rename Folder
function renameFolder(name){
	var body = {'title': $(name).text()},
			folderId = $(name).parent().attr("folder-id");
	gapi.client.drive.files.patch({
		'fileId': folders[folderId].id,
		'resource': body
	}).then(function(){
		getFolders();
	});
}

// Delete Folder
function deleteFolder(){
	$("#folders").css("cursor","progress");
	$(".folder").css("cursor","progress");
	$("body").css("cursor","progress");
	gapi.client.drive.files.delete({
		'fileId': folders[folderId].id
	}).then(function(){
		getFolders();
	});
}


/// Note Functions
// Notes List
function getLinks(folderId){
	$("#folders").css("cursor","progress");
	$(".folder").css("cursor","progress");
	$("body").css("cursor","progress");
	var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
	$.ajax({
		url: folders[folderId].downloadUrl,
		dataType: "json",
		headers: {
			"Authorization": "Bearer " + accessToken
		}
	}).done(function(json){
		$("#dash-title").addClass("links");
		$("#navbar").removeClass("hidden");
		$("#folders").css("cursor","initial");
		$(".folder").css("cursor","pointer");
		$("body").css("cursor","initial");
		$("#folders").css("display","none");
		$("#links").css("display","block");
		$("#dash-title").html(folders[folderId].title);
		$("#folder-options").css("display","inline");
		$("#edit-button").removeClass("hidden");
		$("#delete-button").removeClass("hidden");
		$("#info-button").removeClass("hidden");
		$("#links").html("");
		for(link in json){
			$("#links").append(
				'<li class="link" link-id="'+
				link+'">'+
				'<span class="link-menu">'+
				'<button class="editUrl"></button>'+
				'<a class="openUrl" href="'+
				json[link].url+
				'" target="_blank"></a>'+
				'<button class="removeUrl"></button>'+
				'</span><span class="link-text" contentEditable="true">'+
				json[link].name+
				'</span></li>'
			);
		}
		$("#links").append(
			'<li id="add-link" title="Add a link">'+
			'<form id="link-details" style="display:none;" '+
			'action="#"><input type="text" class="linkName" '+
			'placeholder="Link Name (Optional)" /><input '+
			'type="text" class="linkUrl" placeholder="URL" />'+
			'<button type="submit">Add Link</button></form></li>'
		);
		$("#add-link").one("click", function(){
			addLink(json);
		});
		$(".editUrl").unbind("click").click(function(){
			editUrl($(this).parent().parent().attr("link-id"), json);
		});
		$(".openUrl").unbind("click").click(function(){
			window.open(json[$(this).parent().parent().attr("link-id")].url, "_blank");
		});
		$(".removeUrl").unbind("click").click(function(){
			$(this).parent().parent().css("display", "none");
			removeUrl($(this).parent().parent().attr("link-id"), json);
		});
		$(".link-text").off("blur").on("blur",function(event){
				renameLink($(this).parent().attr("link-id"), json, $(this).text());
		});
		$("#edit-button").unbind("click").click(function(){
			editOpenFolder();
		});
		$("#delete-button").unbind("click").click(function(){
			$("#delete-button").prop("disabled", true);
			deleteFolder();
		});
		$("#info-button").unbind("click").click(function(){
			infoModal();
		});
		$("#links").off("dragover").on("dragover",function(){
			event.preventDefault();
			$(this).addClass("dragging");
		});
		$("#links").off("dragleave").on("dragleave",function(){
			event.preventDefault();
			$(this).removeClass("dragging");
		});
		$("#links").off("drop").on("drop",function(){
			event.preventDefault();
			$(this).removeClass("dragging");
			var result = event.dataTransfer.getData("Text");
			var linktitle = draggingLinkText || result;
			draggingLinkText = false;
			if(result !== "" && isUrl(result)){
				$("#links").css("cursor","progress");
				$(".link").css("cursor","progress");
				var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
				json.push({
					name: linktitle,
					url: result
				});
				var base64Data = btoa(JSON.stringify(json));
				var contentType = "application/json";
				const boundary = '-------314159265358979323846';
				const delimiter = "\r\n--" + boundary + "\r\n";
				const close_delim = "\r\n--" + boundary + "--";
				var multipartRequestBody =
								delimiter +
								'Content-Type: application/json\r\n\r\n' +
								JSON.stringify(folders[folderId]) +
								delimiter +
								'Content-Type: ' + contentType + '\r\n' +
								'Content-Transfer-Encoding: base64\r\n' +
								'\r\n' +
								base64Data +
								close_delim;
				gapi.client.request({
					'path': '/upload/drive/v2/files/' + folders[folderId].id,
					'method': 'PUT',
					'params': {'uploadType': 'multipart', 'alt': 'json'},
					'headers': {
						'Content-Type': 'multipart/mixed; boundary="' + boundary + '"'
					},
					'body': multipartRequestBody
				}).then(function(){
					getLinks(folderId);
					$("#links").css("cursor","initial");
					$(".link").css("cursor","pointer");
				});
			}
		});
	});
}

// Edit Link
function editUrl(link, links){
	toggleCover(true);
	$("#link-edit").css("display", "block");
	$("#link-edit .linkName").val(links[link].name);
	$("#link-edit .linkUrl").val(links[link].url);
	$("#cover").unbind("click").click(function(){
		toggleCover(false);
		$("#link-edit").css("display", "none");
	});
	$("#link-edit").one("submit",function(event){
		event.preventDefault();
		var linkName = $("#link-edit .linkName").val();
		var linkURL = $("#link-edit .linkUrl").val();
		if(linkURL !== undefined){
			if(linkName === undefined)
				linkName = linkURL;
			links[link] = {
				name: linkName,
				url: linkURL
			}
			var base64Data = btoa(JSON.stringify(links));
			var contentType = "application/json";
			const boundary = '-------314159265358979323846';
			const delimiter = "\r\n--" + boundary + "\r\n";
			const close_delim = "\r\n--" + boundary + "--";
			var multipartRequestBody =
							delimiter +
							'Content-Type: application/json\r\n\r\n' +
							JSON.stringify(folders[folderId]) +
							delimiter +
							'Content-Type: ' + contentType + '\r\n' +
							'Content-Transfer-Encoding: base64\r\n' +
							'\r\n' +
							base64Data +
							close_delim;

			gapi.client.request({
				'path': '/upload/drive/v2/files/' + folders[folderId].id,
				'method': 'PUT',
				'params': {'uploadType': 'multipart', 'alt': 'json'},
				'headers': {
					'Content-Type': 'multipart/mixed; boundary="' + boundary + '"'
				},
				'body': multipartRequestBody
			}).then(function(){
				toggleCover(false);
				$("#link-edit").css("display", "none");
				getLinks(folderId);
			});
		}
	});
}

// Remove Link
function removeUrl(link, links){
	links.splice(link, 1);
	var base64Data = btoa(JSON.stringify(links));
	var contentType = "application/json";
	const boundary = '-------314159265358979323846';
	const delimiter = "\r\n--" + boundary + "\r\n";
	const close_delim = "\r\n--" + boundary + "--";
	var multipartRequestBody =
					delimiter +
					'Content-Type: application/json\r\n\r\n' +
					JSON.stringify(folders[folderId]) +
					delimiter +
					'Content-Type: ' + contentType + '\r\n' +
					'Content-Transfer-Encoding: base64\r\n' +
					'\r\n' +
					base64Data +
					close_delim;

	gapi.client.request({
		'path': '/upload/drive/v2/files/' + folders[folderId].id,
		'method': 'PUT',
		'params': {'uploadType': 'multipart', 'alt': 'json'},
		'headers': {
			'Content-Type': 'multipart/mixed; boundary="' + boundary + '"'
		},
		'body': multipartRequestBody
	}).then(function(){
		toggleCover(false);
	});
}

// Add Link
function addLink(links){
	toggleCover(true);
	$("#add-link").addClass("adding-link");
	$("#cover").unbind("click").click(function(){
		toggleCover(false);
		$("#add-link").removeClass("adding-link");
		getLinks(folderId);
	});
	$("#link-details").one("submit",function(event){
		event.preventDefault();
		var linkName = $("#link-details .linkName").val();
		var linkURL = $("#link-details .linkUrl").val();
		if(linkURL !== undefined){
			if(linkName === undefined)
				linkName = linkURL;
			links.push({
				name: linkName,
				url: linkURL
			});
			var base64Data = btoa(JSON.stringify(links));
			var contentType = "application/json";
			const boundary = '-------314159265358979323846';
			const delimiter = "\r\n--" + boundary + "\r\n";
			const close_delim = "\r\n--" + boundary + "--";
			var multipartRequestBody =
							delimiter +
							'Content-Type: application/json\r\n\r\n' +
							JSON.stringify(folders[folderId]) +
							delimiter +
							'Content-Type: ' + contentType + '\r\n' +
							'Content-Transfer-Encoding: base64\r\n' +
							'\r\n' +
							base64Data +
							close_delim;

			gapi.client.request({
				'path': '/upload/drive/v2/files/' + folders[folderId].id,
				'method': 'PUT',
				'params': {'uploadType': 'multipart', 'alt': 'json'},
				'headers': {
					'Content-Type': 'multipart/mixed; boundary="' + boundary + '"'
				},
				'body': multipartRequestBody
			}).then(function(){
				toggleCover(false);
				$("#add-link").removeClass("adding-link");
				getLinks(folderId);
			});
		}
	});
}

// Rename Link
function renameLink(link, links, linkName){
	if(linkName === "")
		return;
	links[link].name = linkName;
	var base64Data = btoa(JSON.stringify(links));
	var contentType = "application/json";
	const boundary = '-------314159265358979323846';
	const delimiter = "\r\n--" + boundary + "\r\n";
	const close_delim = "\r\n--" + boundary + "--";
	var multipartRequestBody =
					delimiter +
					'Content-Type: application/json\r\n\r\n' +
					JSON.stringify(folders[folderId]) +
					delimiter +
					'Content-Type: ' + contentType + '\r\n' +
					'Content-Transfer-Encoding: base64\r\n' +
					'\r\n' +
					base64Data +
					close_delim;

	gapi.client.request({
		'path': '/upload/drive/v2/files/' + folders[folderId].id,
		'method': 'PUT',
		'params': {'uploadType': 'multipart', 'alt': 'json'},
		'headers': {
			'Content-Type': 'multipart/mixed; boundary="' + boundary + '"'
		},
		'body': multipartRequestBody
	}).then(function(data){
		getLinks(folderId);
	});
}

// Essential Documents
function essentials(){
	$("#dash-title").addClass("links");
	$("#folders").css("display","none");
	$("#links").css("display","block");
	$("#navbar").removeClass("hidden");
	$("#folder-options").css("display","inline");
	$("#links").html("");
	$("#dash-title").html("Essentials");
	$("#edit-button").addClass("hidden");
	$("#delete-button").addClass("hidden");
	$("#info-button").addClass("hidden");
	json = {
		"Amendment Sheet": "essentials/AmendmentSheet.pdf",
		"Note Paper": "essentials/NotePaper.pdf",
		"Preambulatory and Operative Clauses": "essentials/PreambulatoryandOperativeClauses.pdf",
		"Resolution format": "essentials/Resolution format.pdf",
		"THIMUN Rules of Procedure": "essentials/THIMUN rules of procedure.pdf",
		"UN4MUN Rules of Procedure": "essentials/UN4MUN rules of procedure.pdf",
		"UNA-USA Rules of Procedure": "essentials/UNA-USA Rules of Procedure.pdf",
		"Universal Declaration of Human Rights": "essentials/Universal Declaration of Human Rights.pdf",
		"ICJ Statute": "essentials/icj_statute_e.pdf",
		"UN Charter": "essentials/uncharter.pdf"
	}
	for(link in json){
		$("#links").append(
			'<li class="pdf">'+
			'<span class="link-menu">'+
			'<a class="openUrl" href="'+
			json[link]+
			'" target="_blank"></a>'+
			'</span>'+
			link+
			'</li>'
		);
	}
}

// Help Modal
function infoModal(){
	toggleCover(true);
	$("#info-modal").removeClass("hidden");
	$("#cover").click(function(){
		toggleCover(false);
		$("#info-modal").addClass("hidden");
	});
}