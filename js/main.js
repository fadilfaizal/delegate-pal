// Variable declarations
var draggingLinkText = false,
		folders = {},
		folderId,
		CLIENT_ID = '541923471598-b06vjtq8oqqo27pph7r7hs4f97jr63ng.apps.googleusercontent.com',
		DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/drive/v2/rest"],
		SCOPES = 'https://www.googleapis.com/auth/drive.appfolder',
		loggedin = true,
		gaRes = {},
		scRes = {},
		secGen = {},
		loggingin = false,
		$window = $(window),
		$body = $("body"),
		$cover = $("#cover"),
		$loggedout = $("#loggedout"),
		$signoutbutton = $("#signout-button"),
		$loggedin = $("#loggedin"),
		$folders = $("#folders"),
		$folder = $(".folder"),
		$card = $(".card"),
		$dash = $("#dash-toggle"),
		$topicInput = $("#topicInput"),
		$root = $('html, body'),
		$profileInput = $("#profileInput"),
		$researchtool = $("#research-tool"),
		$dashboard = $("#dashboard"),
		$cardcountries = $("#cardcountries"),
		$cardsources = $("#cardsources"),
		$searchInner = $("#searchInner"),
		$searchKeys = $("#searchKeys"),
		$searchWiki = $("#searchWiki"),
		$searchOther = $("#searchOther"),
		$ngoInput = $("#ngoInput"),
		$ngoGroupItem = $(".ngoGroupItem"),
		$profileItem = $(".profileItem"),
		$footer = $("footer");

function linkDragStart(){
	draggingLinkText = event.srcElement.innerText;
}

function linkDragEnd(){
	draggingLinkText = false;
}

// Google OAuth Stuff
function initClient() {
	gapi.client.init({
		discoveryDocs: DISCOVERY_DOCS,
		clientId: CLIENT_ID,
		scope: SCOPES
	}).then(function () {
		gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
		updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
	});
}

function toggleCover(makevisible) {
	if(makevisible){
		$cover.css({
			display:"block",
			animation:"fadein 1s"
		});
	} else {
		$cover.css({
			display:"none",
			animation:"none"
		});
	}
}

function updateSigninStatus(isSignedIn) {
	loggedin = isSignedIn;
	if (isSignedIn) {
		$loggedout.css("display", "none");
		$signoutbutton.addClass("signedIn");
		$loggedin.css("display", "block");
		$folders.css("cursor","progress");
		$folder.css("cursor","progress");
		$body.css("cursor","progress");
		$folders.addClass("loading");
		getFolders();
		gapi.client.drive.about.get().execute(function(json){
			var displayName = json.result.user.displayName,
					email = json.result.user.emailAddress,
					firstName = displayName.split(" ")[0];
			if(loggingin){
				var url = "https://getsimpleform.com/messages?form_api_token=7d84cae8f0c910f598519434c33af078",
						body = {
							email: email,
							name: firstName
						}
				$.post(url,body).done();
			}
			heap.identify(email);
			heap.addUserProperties({
				email: email,
				name: displayName
			});
		});
	} else {
		$loggedout.css("display", "block");
		$loggedin.css("display", "none");
		$signoutbutton.removeClass("signedIn");
	}
	footerCheck();
}

function isUrl(s) {
	var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
	return regexp.test(s);
}

function handleClientLoad	(){
	gapi.load('client:auth2', initClient);
}

$window.ready(function(){
	
	var $root = $('html, body');
	var firstClick = true;
	$("#cardswitcher button").one("click",function(){
		var cardclass = $(this).attr("toggle");
		window[cardclass]();
		cardclass = "#" + cardclass;
		$(this).click(function(){
			$card.addClass("hidden");
			$("#cardswitcher button").each(function(){
				$(this).removeClass("selected");
			});
			$(this).addClass("selected");
			$(cardclass).removeClass("hidden");
			$(cardclass + " .scroll").attr("index","1");
			if(/Mobi/.test(navigator.userAgent)){
				if(firstClick){
					$root.animate({
						scrollTop: 0
					},500);
					firstClick = false;
				} else {
					$root.animate({
						scrollTop: $(cardclass).position().top - 15
					},500);
				}
			}
		});
		$(this).click();
		$(".position span").click(function(){
			var index = $(this).index()+1;
			$(this).parent().siblings(".scroll").attr("index",index);
		});
	});
	$("#cardswitcher button[toggle='cardcountries']").click();
	
	// If mobile, else
	if(/Mobi/.test(navigator.userAgent)) {
		$body.addClass("mobile");
		$card.removeClass("collapsed");
		$(".card > *").each(function(){
			$(this).addClass(".noSwipe");
		});
		$.getScript("bower_components/jquery-touchswipe/jquery.touchSwipe.min.js").done(function(){
			$(function(){
				$(".card .scroll").swipe({
					swipeLeft:function() {
						var sections = $(this).children(".section").length;
						var index = $(this).attr("index");
						if(index < sections)
							$(this).attr("index",index*1+1);
						else
							$(this).attr("index",1);
					},
					swipeRight:function() {
						var sections = $(this).children(".section").length;
						var index = $(this).attr("index");
						if(index > 1)
							$(this).attr("index",index*1-1);
						else
							$(this).attr("index",sections);
					}
				});
			});
		});
	}
	
	$(this).resize(function(){
		footerCheck();
	});
	
	var today = new Date();
	var launched = new Date('2017-8-1');
	launched = today.getTime() - launched.getTime();
	launched /= 1000*60*60*24;
	launched = Math.ceil(launched);
	$("#mission #year").html(today.getFullYear());
	$("#contact #active").html(launched);

	$("#research-toggle").click(function(){
		$(".toggled").removeClass("toggled");
		$(this).addClass("toggled");
		$researchtool.css("display", "block");
		$dashboard.css("display", "none");
		footerCheck();
	});

	$dash.one("click", function(){
		$(this).click(function(){
			$(".toggled").removeClass("toggled");
			$(this).addClass("toggled");
			$dashboard.css("display", "block");
			$researchtool.css("display", "none");
			footerCheck();
		});
		$(this).click();
	});

	$dash.on("dragover",function(){
		$(this).click();
	});

	$("#authorize-button").click(function(){
		gapi.auth2.getAuthInstance().signIn();
	});

	$signoutbutton.click(function(){
		gapi.auth2.getAuthInstance().signOut();
	});

	$("#back-button").click(function(){
		$folders.css("cursor","progress");
		$folder.css("cursor","progress");
		$body.css("cursor","progress");
		$folders.addClass("loading");
		getFolders();
	});

	$(".subToggle button").click(function(){
		var column = $(this).parent().parent();
		$(this).siblings().each(function(){
			column.removeClass($(this).attr("toggle"));
		});
		column.addClass($(this).attr("toggle"));
		column.find(".subToggle button.selected").removeClass("selected");
		$(this).addClass("selected");
	});

	$profileInput.on("focus keypress",function(){
		$cardcountries.addClass("empty");
	});

	$topicInput.on("focus keypress",function(){
		$cardsources.addClass("empty");
	});
	
	if ('serviceWorker' in navigator) {
		navigator.serviceWorker.register('/sw.js');
	}
	
});



///// Research Tool

/// Research Cards
// Country Profile
function cardcountries(){
	var $profileList = $("#profileList");
	$profileList.addClass("loading");
	$.getJSON(
		"/json/countries.json"
	).done(function(countries){
		var $profileFacts = $("#profileFacts"),
			$profileLinks = $("#profilePreview #profileLinks"),
			$profileNews = $("#profileNews");
		$profileList.removeClass("loading");
		for(country in countries){
			$profileList.append("<li class='profileItem'>"+country+"</li>");
		}
		$profileItem = $(".profileItem");
		$profileItem.click(function(){
			var country = countries[$(this).html()];
			$("#profileName").html($(this).html());
			$cardcountries.removeClass("empty");
			$profileFacts.html("");
			$profileFacts.addClass("loading");
			$profileNews.html("");
			$profileNews.addClass("loading");
			$profileLinks.html(
				"<div id='previewTitle'>"+$(this).html()+"</div>"+
				(country.Capital?"Capital: "+country.Capital:"")
			);
			var keys = Object.keys(country);
			for(var i=2;i<10;i++){
				if(country[keys[i]])
					$profileLinks.append(
						"<br /><a href='"+country[keys[i]]+"'>"+
						keys[i].replace(/_/g, ' ')+"</a>"
					);
			}
			$("#profilePreview #profileLinks a").attr({
				ondragstart:"linkDragStart()",
				ondragend:"linkDragEnd()",
				target:"_blank"
			});
			$("#profileMap").css("background","url("+country.Image+")");

			if(country.CIA_World_Factbook){
				var code = country.CIA_World_Factbook.split("/");
				code = code[code.length - 1].split(".")[0];
				var ciaurl = "/json/cia/" + code + ".json";
				$.getJSON(ciaurl).done(function(json){
					var geo = json.Geography,
							people = json["People and Society"],
							econ = json.Economy,
							issues = json["Transnational Issues"],
							issuePrint = "<div class='transnational hidden'>";
					$profileFacts.removeClass("loading")
					$profileFacts.html(
						"<button class='collapsenext'>Geography</button>"+
						"<div class='hidden'>"+
						"<b>Location: </b>"+
						geo.Location.text+
						(
							geo["Land boundaries"]["border countries"]?
							"<br /><b>Border countries: </b>"+
							geo["Land boundaries"]["border countries"].text:
							"<br /><b>Border: </b>"+
							geo["Land boundaries"].text
						)+
						"<br /><b>Area: </b>"+
						geo.Area.total.text+
						"<br /><b>Area (comparitive): </b>"+
						geo["Area - comparative"].text+"</div>"+
						"<button class='collapsenext'>People & Society</button>"+
						"<div class='hidden'><b>Population size: </b>"+
						people.Population.text+
						"<br /><b>Population growth rate: </b>"+
						people["Population growth rate"].text+
						(
							people["Median age"]?
							"<br /><b>Median age: </b>"+
							people["Median age"].total.text:
							""
						)+
						"<br /><b>Life expectancy at birth: </b>"+
						people["Life expectancy at birth"]["total population"].text+
						"</div><button class='collapsenext'>Economy</button>"+
						"<div class='hidden'><b>GDP: </b>"+
						econ["GDP (official exchange rate)"].text+
						(
							econ["GDP - real growth rate"]?
							"<br /><b>GDP Growth Rate: </b>"+
							econ["GDP - real growth rate"].text:
							""
						)+
						"<br /><b>GDP Per Capita: </b>"+
						econ["GDP - per capita (PPP)"].text+
						"<br /><b>Budget (Revenues): </b>"+
						econ.Budget.revenues.text+
						"<br /><b>Budget (Expenditures): </b>"+
						econ.Budget.expenditures.text
						+"</div><button class='collapsenext'>Transnational Issues</button>"
					);
					for(issue in issues){
						issuePrint += "<button class='collapsenext'>"+issue+"</button>"
						issuePrint += "<div class='hidden'>";
						if(issues[issue].text === undefined){
							for(subissue in issues[issue]){
								issuePrint += "<b>"+subissue+": </b>"+
									issues[issue][subissue].text+"<br />";
							}
						} else {
							issuePrint += issues[issue].text+"<br />";
						}
						issuePrint += "</div>";
					}
					issuePrint += "</div>";
					$profileFacts.append(issuePrint);
					$("#profileFacts button.collapsenext").click(function(){
						$(this).next().toggleClass("hidden");
					});
				});
			} else {
				$profileFacts.html("No CIA Factbook profile.");
			}

			var gnewsurl = "https://news.google.com/news/rss/search/section/q/";
			gnewsurl += encodeURI($(this).html());
			gnewsurl += "/";
			$.getJSON(
				"https://api.rss2json.com/v1/api.json",
				{
					rss_url: gnewsurl,
					api_key: "eoprbpcphk51nzr9mpxblvncdyozkerqkeryvsol"
				}
			).done(function(json){
				$profileNews.removeClass("loading");
				$profileNews.html("");
				for(item in json.items){
					$profileNews.append(
						'<a style="display:block" target="_blank" href="'+
						json.items[item].link+
						'" ondragstart="linkDragStart()" '+
						'ondragend="linkDragEnd()"><li>'+
						json.items[item].title+
						'</li></a>'
					);
				}
			}).fail(function(){
				$profileNews.removeClass("loading");
				$profileNews.html("Failed to load.");
			});
		});
	});
}

// Topic Background
function cardsources(){
	var $sourceLinks = $("#sourceLinks"),
			$relatedNews = $("#relatedNews"),
			$relatedVideos = $("#relatedVideos");
	$sourceLinks.addClass("loading");
	$.getJSON(
		"/json/topic.json"
	).done(function(sources){
		$sourceLinks.removeClass("loading");
		for(section in sources){
			$sourceLinks.append("<button class='collapsenext'>"+section+"</button>");
			var toAppend = "<div class='hidden'>";
			section = sources[section];
			for(item in section){
				toAppend += "<li class='sourceItem' gotourl='"+
					section[item].Search+
					"'"+(section[item].Plus?" plus='true'":" plus='false'")+
					">"+
					item+"</li>";
			}
			toAppend += "</div>";
			$sourceLinks.append(toAppend);
		}
		$("#sourceLinks button.collapsenext").click(function(){
			$(this).next().toggleClass("hidden");
		});
		
		$(".sourceItem").click(function(){
			var finalURL = $(this).attr("gotourl");
			var searchValue = $topicInput.val();
			if($(this).attr("plus") === "true")
				searchValue = searchValue.replace(/ /g,"+");
			finalURL += encodeURI(searchValue);
			window.open(finalURL, '_blank');
		});
	});
	
	$("#snippetForm").submit(function(event){
		event.preventDefault();
		$searchInner.html("");
		$searchKeys.html("");
		$searchWiki.addClass("loading");
		$relatedNews.html("");
		$relatedNews.addClass("loading");
		$relatedVideos.html("");
		$relatedVideos.addClass("loading");
		// If valid input
		if($topicInput.val() !== ""){
			$topicInput.blur();
			$cardsources.removeClass("empty");
			var query = $topicInput.val();
			$searchWiki.removeClass("empty");
			$searchWiki.find("h3").html("");
			$searchWiki.find("#searchKeys").html("");
			
			// Wikipedia Results
			$.getJSON(
				"https://en.wikipedia.org/w/api.php",
				{
					action: "query",
					list: "search",
					srprop: null,
					srlimit: 10,
					utf8: null,
					origin: "*",
					format: "json",
					srsearch: $topicInput.val()
				}
			).done(function(wsearch){
				if(wsearch.query.searchinfo.totalhits === 0){
					$searchWiki.removeClass("empty");
					$searchWiki.find("h3").html("No results found.");
					$searchInner.html("");
					$searchKeys.html("");
					return;
				} else if(false){
					var suggested = wsearch.query.searchinfo.suggested;
					// Search suggestion
					$.getJSON(
						"https://en.wikipedia.org/w/api.php",
						{
							action: "query",
							list: "search",
							srprop: null,
							srlimit: 10,
							utf8: null,
							origin: "*",
							format: "json",
							srsearch: $topicInput.val()
						}
					).done(function(wsearch){
						wsearch = wsearch.query.search;
						search = wsearch[0].title;
						$searchOther.html("");
						for(item in wsearch){
							if(item === 0){
								$searchOther.append(
									"<button onclick='changeSearch(false)'>"+
									wsearch[item].title+
									"</button>"
								);
							} else {
								$searchOther.append(
									"<button onclick='wikiApi(this.innerHTML)'>"+
									wsearch[item].title+
									"</button>"
								);
							}
						}
						wikiApi(search);
					});
				} else {
					wsearch = wsearch.query.search;
					search = wsearch[0].title;
					$searchOther.html("");
					for(item in wsearch){
						if(item === 0){
							$searchOther.append(
								"<button onclick='changeSearch(false)'>"+
								wsearch[item].title+
								"</button>"
							);
						} else {
							$searchOther.append(
								"<button onclick='wikiApi(this.innerHTML)'>"+
								wsearch[item].title+
								"</button>"
							);
						}
					}
					wikiApi(search);
				}
			}).fail(function(){
				$searchWiki.removeClass("loading");
				$searchWiki.html("Failed to load.");
			});
			var newsurl = "https://news.google.com/news/rss/search/section/q/"+
					encodeURI(query)+"/"+encodeURI(query);
			
			// Google News
			$.getJSON(
				"https://api.rss2json.com/v1/api.json",
				{
					rss_url: newsurl,
					api_key: "eoprbpcphk51nzr9mpxblvncdyozkerqkeryvsol"
				}
			).done(function(json){
				$relatedNews.removeClass("loading");
				for(item in json.items){
					$relatedNews.append(
						'<a target="_blank" href="'+
						json.items[item].link+
						'" ondragstart="linkDragStart()" '+
						'ondragend="linkDragEnd()"'+
						'><li>'+
						json.items[item].title+
						'</li></a>'
					);
				}
			}).fail(function(){
				$relatedNews.removeClass("loading");
				$relatedNews.html("Failed to load.");
			});
	
			
			// Youtube Results
			$.getJSON(
				"https://www.googleapis.com/youtube/v3/search",
				{
					key: "AIzaSyBSwl-6evitkGxGmiuN8zT15Yze5w5uBi4",
					part: "snippet",
					maxResults: "10",
					type: "video",
					regionCode: "US",
					q: query
				}
			).done(function(results){
				$relatedVideos.removeClass("loading");
				results = results.items;
				for(item in results){
					$relatedVideos.append(
						"<a target='_blank' href='"+
						"https://www.youtube.com/watch?v="+
						results[item].id.videoId+
						"' class='videoItem'><li>"+
						"<span style='background-image:url("+
						results[item].snippet.thumbnails.default.url+
						")'></span>"+
						results[item].snippet.title+
						"</li></a>"
					);
				}
			}).fail(function(){
				$relatedVideos.removeClass("loading");
				$relatedVideos.html("Failed to load.");
			});
		}
	});
}

// Resolution Search
function cardres(){
	var $gaRes = $("#gaRes"),
			$scRes = $("#scRes"),
			$secGen = $("#secGen"),
			$resInput = $("#resInput");
	
	$gaRes.addClass("loading");
	$scRes.addClass("loading");
	$secGen.addClass("loading");
	
	$.getJSON(
		"/json/res/ga.json"
	).done(function(json){
		gaRes = json;
		$gaRes.removeClass("loading");
		searchFilter($resInput.val(),$gaRes);
		$resInput.on("keyup",function(){
			searchFilter($resInput.val(),$gaRes);
		});
	});

	$.getJSON(
		"/json/res/sc.json"
	).done(function(json){
		scRes = json;
		$scRes.removeClass("loading");
		searchFilter($resInput.val(),$scRes);
		$resInput.on("keyup",function(){
			searchFilter($resInput.val(),$scRes);
		});
	});

	$.getJSON(
		"/json/res/sg.json"
	).done(function(json){
		secGen = json;
		$secGen.removeClass("loading");
		searchFilter($resInput.val(),$secGen);
		$resInput.on("keyup",function(){
			searchFilter($resInput.val(),$secGen);
		});
	});
}

// UN Directory
function cardlinks(){
	var $unLevel1 = $("#unLevel1"),
			$unLevel2 = $("#unLevel2"),
			$unLevel3 = $("#unLevel3");
	$unLevel1.addClass("loading");
	$.getJSON(
		"/json/unLinks.json"
	).done(function(links){
		$unLevel1.removeClass("loading");
		for(linkGroup in links){
			$unLevel1.append(
				"<li class='unGroupItem'>"+
				linkGroup+"</li>"
			);
		}
		$(".unGroupItem").click(function(){
			$("#cardlinks .scroll").attr("index","2");
			$unLevel2.removeClass("empty");
			$(".unGroupItem.selected").removeClass("selected");
			$(this).addClass("selected");
			$unLevel2.html("");
			$unLevel3.html("");
			var linkGroup = $(this).text();
			var linkList = links[linkGroup];
			for(link in linkList){
				if(typeof linkList[link] === "string"){
					$unLevel2.append(
						"<a href='"+
						linkList[link]+
						"' target='_blank' "+
						"ondragstart='linkDragStart()' "+
						"ondragend='linkDragEnd()'>"+
						"<li>"+
						link+
						"</li></a>"
					);
				} else {
					$unLevel2.append("<li class='unItem'>"+link+"</li>");
				}
			}
			$unLevel2.children(".unItem").click(function(){
				$("#cardlinks .scroll").attr("index","3");
				$(".unItem.selected").removeClass("selected");
				$(this).addClass("selected");
				var linkValue = linkList[$(this).text()];
				$unLevel3.html("");
				for(finalLink in linkValue){
					$unLevel3.append(
						"<a href='"+
						linkValue[finalLink]+
						"' target='_blank' "+
						"ondragstart='linkDragStart()' "+
						"ondragend='linkDragEnd()'>"+
						"<li class='unBodyItem'>"+
						finalLink+
						"</li></a>"
					);
				}
			});
		});
	});
}

// Treaties and Conventions
function cardtreaties(){
	var $treatyCategories = $("#treatyCategories"),
			$treatyTitles = $("#treatyTitles"),
			$wikiSum = $("#wikiSum"),
			$wikiProp = $("#wikiProp");
	$treatyCategories.addClass("loading");
	$.getJSON(
		"/json/treaties.json"
	).done(function(treaties){
		$treatyCategories.removeClass("loading");
		for(category in treaties){
			$treatyCategories.append(
				"<li class='categoryItem'>"+
				category+"</li>"
			);
		}
		$(".categoryItem").click(function(){
			$("#cardtreaties .scroll").attr("index","2");
			$treatyTitles.removeClass("empty");
			$(".categoryItem.selected").removeClass("selected");
			$(this).addClass("selected");
			$treatyTitles.html("");
			var category = $(this).text();
			var linkList = treaties[category];
			for(link in linkList){
				if(typeof linkList[link] === "string"){
					$treatyTitles.append(
						"<a href='"+
						linkList[link]+
						"' target='_blank' "+
						"ondragstart='linkDragStart()' "+
						"ondragend='linkDragEnd()'>"+
						"<li>"+
						link+
						"</li></a>"
					);
				} else {
					$treatyTitles.append("<li class='treatyLinkItem'>"+link+"</li>");
				}
			}
			$treatyTitles.children(".treatyLinkItem").click(function(){
				$("#cardtreaties .scroll").attr("index","3");
				$("#treatyProfile").removeClass("empty");
				$(".treatyLinkItem.selected").removeClass("selected");
				$(this).addClass("selected");
				var treaty = linkList[$(this).text()];
				$wikiSum.html(
					"<h3>"+
					$(this).text()+
					"</h3>"+
					treaty.intro
				);
				$wikiProp.html(
					(
						treaty.prop ?
						treaty.prop:
						"No details available"
					)
				);
				$wikiSum.append(
					"<a href='https://en.wikipedia.org/wiki/"+
					treaty.url+
					"' target='_blank' "+
					"ondragstart='linkDragStart()' "+
					"class='readmore' "+
					"ondragend='linkDragEnd()'>Read More</a>"
				);
				$wikiPropA = $wikiProp.find("a");
				$wikiPropA.attr("target","_blank");
				$wikiPropA.each(function(){
					$(this).attr(
						"href",
						"https://en.wikipedia.org"+
						$(this).attr("href")
					);
				})
			});
		});
	});
}

// NGO Directory
function cardngos(){
	var selected = false,
			$ngoGroupList = $("#ngoGroupList"),
			$ngoPreview = $("#ngoPreview");
	$ngoGroupList.addClass("loading");
	$.getJSON(
		"/json/ngoCategories.json"
	).done(function(categories){
		$ngoGroupList.removeClass("loading");
		for(category in categories){
			$ngoGroupList.append(
				"<li class='ngoGroupItem'>"+
				category+"</li>"
			);
		}
		$ngoGroupItem = $(".ngoGroupItem");
		$ngoGroupItem.click(function(){
			selected = this;
			$ngoPreview.addClass("loading");
		});
		$.getJSON(
			"/json/ngos.json"
		).done(function(ngos){
			$ngoGroupItem.unbind("click").click(function(){
				$("#cardngos .scroll").attr("index","2");
				$(".ngoGroupItem.selected").removeClass("selected");
				$(this).addClass("selected");
				updateNGOs();
				$ngoPreview.removeClass("empty");
				var category = categories[$(this).text()];
				$ngoPreview.html("");
				for(ngo in category){
					$ngoPreview.append(
						"<a target='_blank' "+
						"ondragstart='linkDragStart()' "+
						"ondragend='linkDragEnd()' "+
						"href='http://esango.un.org/civilsociety/showProfileDetail.do"+
						"?method=showProfileDetails&sessionCheck=false&profileCode="+
						ngos[category[ngo]]+
						"'><li class='ngoItem'>"+
						category[ngo]+"</li></a>"
					);
				}
			});
			if(selected){
				$(selected).click();
				$ngoPreview.removeClass("loading");
			}
		});
	});
}

// Latest Headlines
function cardheadlines(){
	var $ReutersHeadlines = $("#ReutersHeadlines"),
		$GNewsHeadlines = $("#GNewsHeadlines"),
		$UNHeadlines = $("#UNHeadlines");
	
	$ReutersHeadlines.addClass("loading");
	$GNewsHeadlines.addClass("loading");
	$UNHeadlines.addClass("loading");

	// Reuters World News Feed
	$.getJSON(
		"https://api.rss2json.com/v1/api.json",
		{
			rss_url: "http://feeds.reuters.com/Reuters/worldNews",
			api_key: "eoprbpcphk51nzr9mpxblvncdyozkerqkeryvsol"
		}
	).done(function(json){
		$ReutersHeadlines.removeClass("loading");
		for(item in json.items){
			$ReutersHeadlines.append(
				'<a target="_blank" href="'+
				json.items[item].link+
				'" ondragstart="linkDragStart()" '+
				'ondragend="linkDragEnd()"'+
				'><li>'+
				json.items[item].title+
				'</li></a>'
			);
		}
	}).fail(function(){
		$ReutersHeadlines.append("Failed to load.");
	});

	// Google News (World) Feed
	$.getJSON(
		"https://api.rss2json.com/v1/api.json",
		{
			rss_url: "https://news.google.com/news/rss/headlines/section/topic/WORLD",
			api_key: "eoprbpcphk51nzr9mpxblvncdyozkerqkeryvsol"
		}
	).done(function(json){
		$GNewsHeadlines.removeClass("loading");
		for(item in json.items){
			$GNewsHeadlines.append(
				'<a target="_blank" href="'+
				json.items[item].link+
				'" ondragstart="linkDragStart()" '+
				'ondragend="linkDragEnd()"'+
				'><li>'+
				json.items[item].title+
				'</li></a>'
			);
		}
	}).fail(function(){
		$GNewsHeadlines.append("Failed to load.");
	});

	// UN News RSS Feed
	$.getJSON(
		"https://api.rss2json.com/v1/api.json",
		{
			rss_url: "http://www.un.org/apps/news/rss/rss_top.asp",
			api_key: "eoprbpcphk51nzr9mpxblvncdyozkerqkeryvsol"
		}
	).done(function(json){
		$UNHeadlines.removeClass("loading");
		for(item in json.items){
			$UNHeadlines.append(
				'<a target="_blank" href="'+
				json.items[item].link+
				'" ondragstart="linkDragStart()" '+
				'ondragend="linkDragEnd()"'+
				'><li>'+
				json.items[item].title+
				'</li></a>'
			);
		}
	}).fail(function(){
		$UNHeadlines.append("Failed to load.");
	});
}

// Resource Bank
function cardresources(){
	var $resourceGroups = $("#resourceGroups"),
			$resourceLinks = $("#resourceLinks");
	
	$resourceGroups.addClass("loading");
	$.getJSON(
		"/json/resources.json"
	).done(function(resources){
		$resourceGroups.removeClass("loading");
		for(resourceGroup in resources){
			$resourceGroups.append(
				"<li class='resourceGroupItem'>"+
				resourceGroup+"</li>"
			);
		}
		$(".resourceGroupItem").click(function(){
			$("#cardresources .scroll").attr("index","2");
			$(".resourceGroupItem.selected").removeClass("selected");
			$(this).addClass("selected");
			$resourceLinks.removeClass("empty");
			$resourceLinks.html("");
			var resourcelist = resources[$(this).text()];
			for(item in resourcelist){
				$resourceLinks.append(
					"<a ondragstart='linkDragStart()' "+
					"ondragend='linkDragEnd()' target='_blank' href='"+
					resourcelist[item]+
					"'><li class='resourceItem'>"+
					item+"</li><a/>"
				);
			}
		});
	});
}

// Feedback Card
function cardfeedback(){
	$("#feedbackForm").submit(function(event){
		event.preventDefault();
		if($("#cardfeedback [name='message']").val().trim() !== "");
		$.post(
			"https://getsimpleform.com/messages?form_api_token=03da4384c598542b5bf374c8e0d7c136",
			{
				message: $("#cardfeedback [name='message']").val(),
				content: $("#cardfeedback [name='content']").val()
			}
		).done(function(){
			$(this).remove();
			$("#cardfeedback").append(
				"<p>Thank you for your feedback!</p>"
			);
		});
	});
}


/// Extra Functions
// NGO Filter
function updateNGOs(){
	var input = $ngoInput.val();
	$ngoGroupItem.each(function(){
		if($(this).text().toUpperCase().indexOf(input.toUpperCase()) !== -1){
			$(this).css("display","list-item");
		} else {
			$(this).css("display","none");
		}
	});
}

// Country Filter
function updateList(){
	var input = $profileInput.val();
	$profileItem.each(function(){
		if($(this).text().toUpperCase().indexOf(input.toUpperCase()) !== -1){
			$(this).css("display","list-item");
		} else {
			$(this).css("display","none");
		}
	});
}

// Search Filter
function searchFilter(needle, $identifier){
	if(needle === ""){
		return false;
	}
	var text = $identifier.find("h3").text();
	switch(text){
		case "General Assembly":
			var prefix = "http://www.un.org/en/ga/search/view_doc.asp?symbol=A/RES/",
					haystack = gaRes;
			break;
		case "Security Council":
			var prefix = "http://www.un.org/en/ga/search/view_doc.asp?symbol=S/RES/",
					haystack = scRes;
			break;
		case "SG Reports":
			var prefix = "http://www.un.org/en/ga/search/view_doc.asp?symbol=S/",
					haystack = secGen;
			break;
	}
	$identifier.addClass("loading");
	if(Object.keys(haystack).length === 0){
		setTimeout(function(){
			searchFilter(needle, $identifier);
		},500);
		return;
	}
	$identifier.removeClass("loading");
	$identifier.children("div,a").remove();
	var qarray = needle.toLowerCase().match(/\w+/g),
		toremove = [
			"and",
			"of",
			"the",
			"on",
			"for",
			"with",
			"to",
			"a",
			"an",
			"in",
			"its",
			"may",
			"be",
			"under",
			"which",
			"when",
			"where",
			"between",
			"full",
			"by",
			"from",
			"with",
			"at",
			"against",
			"draft",
			"drafts",
			"including",
			"strengthening",
			"place",
			"places",
			"meeting",
			"meetings",
			"session",
			"sessions",
			"service",
			"services",
			"headquarters",
			"utilization",
			"future",
			"convention",
			"co-ordination",
			"preparation",
			"preparations",
			"part",
			"registration",
			"supplementary",
			"appointment",
			"appointments",
			"principle",
			"principles",
			"proposal",
			"proposals",
			"fill",
			"vacancy",
			"vacancies",
			"proposed",
			"likely",
			"disturb",
			"enquiry",
			"implementation",
			"recommendation",
			"recommendations",
			"problem",
			"problems",
			"measures",
			"threat",
			"threats",
			"question",
			"questions",
			"practiced",
			"study",
			"other",
			"method",
			"methods",
			"exercised",
			"present",
			"affecting",
			"regard",
			"bringing",
			"given",
			"topic",
			"topics",
			"covering",
			"advancement",
			"pursue",
			"pursuance",
			"ended",
			"purpose",
			"consider",
			"considered",
			"relating",
			"confirmation",
			"list",
			"lists",
			"impeding",
			"trend",
			"trends",
			"review",
			"reviews",
			"conclusion",
			"conclusions",
			"enlargement",
			"study",
			"studies"
		],
		newarray = [];
	for(index in qarray){
		// useless words in array
		if(toremove.indexOf(qarray[index]) === -1)
			newarray.push(qarray[index]);
	}
	qarray = newarray;
	newarray = {}
	for(bale in haystack){
		var clean = bale.toLowerCase().match(/\w+/g),
				rank = 0;
		for(word in clean){
			if(qarray.indexOf(clean[word]) !== -1){
				rank++;
			}
		}
		if(rank !== 0){
			if(newarray[rank] === undefined){
				newarray[rank] = {}
			}
			newarray[rank][bale] = haystack[bale];
		}
	}
	var keys = Object.keys(newarray).sort().reverse();
	if(keys.length === 0){
		$identifier.append(
			"<div style='padding:1em 0'>"+
			"No results found.</div>"
		);
		return false;
	}
	for(rank in keys){
		rank = keys[rank];
		for(bale in newarray[rank]){
			var print = bale;
			for(word in qarray){
				var reg = new RegExp("\\b"+qarray[word]+"\\b","gi");
				print = print.replace(
					reg,
					function(x){
						return "<span>"+x+"</span>"
					}
				)
			}
			$identifier.append(
				"<a target='_blank' "+
				"ondragstart='linkDragStart()' "+
				"ondragend='linkDragEnd()' "+
				"href='"+prefix+
				newarray[rank][bale]+
				"'><li class='resolutionItem'>"+
				print+"</li></a>"
			);
		}
	}
}

// Topic Background Wikipedia Snippet
function wikiApi(search){
	changeSearch(false);
	$searchInner.html("");
	$searchKeys.html("");
	$searchWiki.addClass("loading");
	$.getJSON(
		"https://en.wikipedia.org/w/api.php",
		{
			action: "parse",
			prop: "text|links",
			section: 0,
			format: "json",
			origin: "*",
			page: search
		}
	).done(function(wiki){
		$searchWiki.removeClass("loading");
		$searchWiki.removeClass("empty");
		$("#searchWiki h3").html(wiki.parse.title);
		var links = wiki.parse.links;
		wiki = $.parseHTML(wiki.parse.text["*"]);
		$(wiki).find("#coordinates").parent().parent().remove();
		$(wiki).find('a[href^="#"]').remove();
		$(wiki).find("a").each(function(){
			$(this).attr(
				"href",
				"https://en.wikipedia.org"+
				$(this).attr("href")
			);
			$(this).attr("target","_blank");
		});
		var abstract = $(wiki).children("p").first();

		// If 'disambiguation' article
		if(abstract.text().match(/(may refer to:|may also refer to:|Choose category your search belongs to:|may belong to any of the following categories.|usually refers to:)/)){
			changeSearch(true);
			$("#searchOther button").eq(0).remove();
		} else {
			$("#searchWiki #searchInner").html(abstract.text());
			if(abstract.text().match(/usually refers to:/)){
				$("#searchWiki #searchInner").append(abstract.next().html());
			}
			$("#searchWiki #searchInner").append(
				"<br /><a target='_blank' href='"+
				"https://en.wikipedia.org/wiki/"+
				$("#searchWiki h3").text()+
				"' ondragstart='linkDragStart()' "+
				'ondragend="linkDragEnd()"'+
				"' style='margin:1em'>"+
				"Read More</a><button onclick='changeSearch(true)'>"+
				"Not what you were looking for?</button>"
			);
			$("#searchWiki #searchKeys").html("");
		}
		for(link in links){
			if(links[link]["*"].indexOf(":") === -1){
				$searchKeys.append(
					'<a href="'+
					'https://en.wikipedia.org/wiki/'+
					links[link]["*"]+
					'" ondragstart="linkDragStart()" '+
					'ondragend="linkDragEnd()"'+
					' target="_blank">'+
					links[link]["*"]+
					"</a>"
				);
			}
		}
	});
}

// Change Query Result
function changeSearch(open){
	if(open){
		$searchOther.removeClass("hidden");
		$searchInner.addClass("hidden");
		$searchKeys.addClass("hidden");
		$("#searchWiki h4").addClass("hidden");
	} else {
		$searchOther.addClass("hidden");
		$searchInner.removeClass("hidden");
		$searchKeys.removeClass("hidden");
		$("#searchWiki h4").removeClass("hidden");
	}
}

// Footer Handler
function footerCheck(){
	var footerHeight = $footer.outerHeight(),
			topOffset = $footer.offset().top - ($footer.css("margin-top").slice(0,-2)*1),
			totalHeight = footerHeight + topOffset + 140;
	if(totalHeight < $window.height()){
		newmargin = ($window.height() - totalHeight + 140)+
			"px";
		$footer.css(
			"margin-top",
			newmargin
		)
	} else {
		$footer.css("margin-top", "140px");
	}
}